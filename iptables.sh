#!/bin/sh

iptables -F
iptables -X
iptables -F -t nat
iptables -X -t nat

iptables -P INPUT   DROP
iptables -P OUTPUT  DROP
iptables -P FORWARD DROP

ipset destroy

d0="$(dirname "$0")"

#=======================================================================
#
#  Routines
#

Foreach_ipaddr() {
	local f="$1"; shift
	egrep -v -- '^( *| *#.*)$' "$f" | while read line; do "$@" $line; done
}

File2ipset() {
	local f="$1" s="$2"
	shift; shift
	ipset create "$s" "$@"
	Foreach_ipaddr "$f" ipset add "$s"
}

Accept() { local chain="$1"; shift; iptables -A "$chain" -j ACCEPT "$@"; }
Drop()   { local chain="$1"; shift; iptables -A "$chain" -j DROP   "$@"; }

New_chain() {
	local chain="$1"; shift
	iptables -N "$chain"
	iptables -A "$@" -j "$chain"
}

Todo() { : 'todo!!!' ; }

#=======================================================================
#
#  Input
#

Accept      INPUT -i lo
Drop        INPUT -m state --state INVALID
Drop        INPUT -p tcp   --dport 445
Accept      INPUT -m state --state RELATED,ESTABLISHED

File2ipset  "$d0/input_accept.lst"   Allowed_input_sources hash:net
Accept      INPUT -m set --match-set Allowed_input_sources src

#=======================================================================
#
#  Output
#

Accept OUTPUT -m state --state RELATED,ESTABLISHED
Accept OUTPUT -o lo
Accept OUTPUT -p icmp

#=======================================================================
#
#  Forward start
#

#ptables -A FORWARD -p tcp --tcp-flags SYN,RST SYN -j TCPMSS --clamp-mss-to-pmtu

Accept FORWARD  -m state --state RELATED,ESTABLISHED

Accept FORWARD  -i ens2f1    -o ens2f0
Accept FORWARD  -i ens2f1    -o enp3s0f0
Accept FORWARD  -i enp3s0f0  -o ens2f0
Accept FORWARD  -i enp3s0f0  -o ens2f1
#ccept FORWARD  -i enp3s0f0  -o ens2f1

New_chain  f0_f1              FORWARD    -i ens2f0 -o ens2f1
New_chain  f0_3s              FORWARD    -i ens2f0 -o enp3s0f0
New_chain  f0_3s_udp          f0_3s      -p udp
New_chain  f0_f1_udp          f0_f1      -p udp
New_chain  f0_f1_tcp          f0_f1      -p tcp
New_chain  f0_f1_tcp_decho    f0_f1_tcp  -p tcp --dport echo
New_chain  f0_f1_tcp_d22      f0_f1_tcp  -p tcp --dport 22
New_chain  f0_f1_tcp_d80      f0_f1_tcp  -p tcp --dport 80
New_chain  f0_f1_tcp_d90      f0_f1_tcp  -p tcp --dport 90
New_chain  f0_f1_tcp_d443     f0_f1_tcp  -p tcp --dport 443
New_chain  f0_f1_tcp_d1935    f0_f1_tcp  -p tcp --dport 1935
New_chain  f0_f1_tcp_d80_443  f0_f1_tcp  -p tcp --match multiport --dports 80,443
New_chain  f0_f1_tcp_d80_1935 f0_f1_tcp  -p tcp --match multiport --dports 80,1935
New_chain  f0_f1_icmp         f0_f1      -p icmp
New_chain  f0_f1_udp_d10k_20k f0_f1_udp  -p udp --dport 10000:20000

New_chain  f0_f1_udp_dst_65.51.67.112   f0_f1_udp -d 65.51.67.112


#=======================================================================
#
#  Forward rules
#

File2ipset "$d0/forward_reject.lst" Reject_forward_sources hash:net
iptables -A FORWARD -j REJECT --reject-with icmp-host-prohibited \
    -m set --match-set Reject_forward_sources src

Accept FORWARD -s 108.54.168.238
Accept FORWARD -s 27.116.17.224/27
Accept FORWARD -s 209.95.39.70
Accept FORWARD -s 209.95.39.78

Accept f0_f1   -s 122.252.246.66
Accept f0_3s   -s 122.252.246.66
Accept f0_f1   -s 112.133.238.88/29
Accept f0_3s   -s 112.133.238.88/29
Accept f0_f1   -s 122.252.230.160/28
Accept f0_3s   -s 122.252.230.160/28


############ 5centscdn
New_chain  dst_65.51.67.40   f0_f1  -d 65.51.67.40
Accept     dst_65.51.67.40  -s 209.95.44.21
Accept     dst_65.51.67.40  -s 209.95.44.20
Accept     dst_65.51.67.40  -s 67.159.54.42
Accept     dst_65.51.67.40  -s 167.114.102.37

#### TV5 studio in india
Accept  f0_f1      -s 115.248.86.171 -d 65.51.67.245
#### Amazon testing voip server
Accept  f0_f1_udp_dst_65.51.67.112   -s 52.39.1.232
####### sputnik server
Accept  f0_f1_tcp                    -d 65.51.67.194 -p tcp --dport 5111


#### new mail server by shree
Accept  f0_f1_tcp  -d 65.51.67.23  -p tcp --match multiport  --dports 22,25,80,465,587,2525,143,993,8083
Accept  f0_f1_tcp  -d 65.51.67.109 -p tcp --match multiport  --dports 22,80,443
Accept  f0_f1_udp  -d 65.51.67.109


######### Andrey

New_chain  dst_65.51.67.124   f0_f1 -d 65.51.67.124
New_chain  dst_65.51.67.125   f0_f1 -d 65.51.67.125

Accept  dst_65.51.67.124  -s 213.177.105.58
Accept  dst_65.51.67.124  -s 89.109.34.90
Accept  dst_65.51.67.124  -s 82.208.93.50
Accept  dst_65.51.67.124                     -p tcp --dport 888
Accept  dst_65.51.67.124                     -p tcp --dport 20223

Accept  dst_65.51.67.125                     -p tcp --dport 888
Accept  dst_65.51.67.125  -s 213.177.105.58
Accept  dst_65.51.67.125  -s 89.109.34.90
Accept  dst_65.51.67.125  -s 82.208.93.50


######### FTP for Sunil on 17/2/2015
Accept  f0_f1  -s 174.142.220.100 -d 65.51.67.247
Accept  f0_f1  -s 108.5.148.35    -d 65.51.67.247
Accept  f0_f1  -s 119.235.48.34   -d 65.51.67.209

Accept  f0_f1_udp          -d 65.51.67.36
Accept  f0_f1_udp          -d 65.51.67.37
Accept  f0_f1_udp          -d 65.51.67.146
Accept  f0_f1_udp          -d 65.51.67.148
Accept  f0_f1_udp          -d 65.51.67.39

Accept  f0_f1_tcp          -d 65.51.67.39    -p tcp --dport 8661
Accept  f0_f1_tcp_d80      -d 65.51.67.11
Accept  f0_f1_tcp_d80      -d 65.51.67.124
Accept  f0_f1_tcp_d80      -d 65.51.67.125
Accept  f0_f1_tcp_d80      -d 65.51.67.185
Accept  f0_f1_tcp_d80      -d 65.51.67.240
Accept  f0_f1_tcp_d90      -d 65.51.67.119
Accept  f0_f1_tcp_d80_1935 -d 65.51.67.119
Accept  f0_f1_tcp_d80_1935 -d 65.51.67.127
#ccept  f0_f1_udp          -d 65.51.67.119
#########
#ccept  f0_f1_tcp   -s 108.54.168.238 -d 65.51.67.119


######### ILD
#Accept f0_f1_udp  -d 65.51.67.98
#Accept f0_f1_udp  -d 65.51.67.99
#Accept f0_f1_udp  -d 65.51.67.101
#Accept f0_f1_udp  -d 65.51.67.102
#Accept f0_f1_udp  -d 65.51.67.104
#Accept f0_f1_udp  -d 65.51.67.166
#Accept f0_f1_udp  -d 65.51.67.178
#Accept f0_f1_udp  -d 65.51.67.179

Accept f0_3s_udp  -d 65.51.243.114
Accept f0_3s_udp  -d 65.51.243.115
Accept f0_3s_udp  -d 65.51.243.116
Accept f0_3s_udp  -d 65.51.243.117
Accept f0_3s_udp  -d 65.51.243.118
Accept f0_3s_udp  -d 65.51.243.119
Accept f0_3s_udp  -d 65.51.243.120
Accept f0_3s_udp  -d 65.51.243.121
Accept f0_3s_udp  -d 65.51.243.122
#ccept f0_3s_tcp  -d 65.51.243.123 -p tcp --dport 22
Accept f0_f1_udp  -d 65.51.67.126
Accept f0_f1_udp  -d 65.51.67.128
Accept f0_f1_udp  -d 65.51.67.137
Accept f0_f1_udp  -d 65.51.67.138
Accept f0_f1_udp  -d 65.51.67.140
#ccept f0_f1_tcp  -s 119.235.52.218 -d 65.51.67.185 -p tcp --dport 22
Accept f0_f1_udp  -d 65.51.67.222
Accept f0_f1_tcp_d80  -s 98.221.80.60 -d 65.51.67.112
Accept f0_f1_tcp_d443 -s 98.221.80.60 -d 65.51.67.112

####### Simple Network ILD
Accept f0_f1_udp_dst_65.51.67.112  -s 216.1.185.100
Accept f0_f1_udp_dst_65.51.67.112  -s 216.1.185.103
Accept f0_f1_udp                   -s 216.1.185.100   -d 65.51.67.222
Accept f0_f1_udp                   -s 216.1.185.103   -d 65.51.67.222
Accept f0_f1_udp_dst_65.51.67.112  -s 198.105.220.68
Accept f0_f1_udp_dst_65.51.67.112  -s 216.155.155.6
Accept f0_f1_udp_dst_65.51.67.112  -s 216.155.155.5
Accept f0_f1_udp                   -s 216.155.155.5   -d 65.51.67.66

Accept  f0_f1      -s 216.155.155.5   -d 65.51.67.12
Accept  f0_f1      -s 216.155.155.5   -d 65.51.67.147
Accept  f0_f1      -s 206.190.152.214 -d 65.51.67.240
######### ILD customer SAHASRA 29042014 ######
Accept  f0_f1_udp  -s 192.95.49.239   -d 65.51.67.112
######### shree home
Accept  f0_f1      -s 69.122.68.135
#ccept  f0_f1      -s 24.188.42.236
#ccept  f0_f1      -s 24.187.222.218
#ccept  f0_f1      -s 103.227.245.20 -d 65.51.67.140

######### added for tv5 by shree
Accept  f0_f1_tcp        -d 65.51.67.133 -p tcp --dport 22
Accept  f0_f1_tcp_d1935  -d 65.51.67.133

######### Global Tele Services asked by sainath
Accept  f0_f1_udp_dst_65.51.67.112  -s 208.43.230.211
Accept  f0_f1_udp_dst_65.51.67.112  -s 208.43.230.210
Accept  f0_f1_udp_dst_65.51.67.112  -s 50.22.197.99
Accept  f0_f1_udp_dst_65.51.67.112  -s 208.43.226.37
######### GTS 28012015
Accept  f0_f1_udp_dst_65.51.67.112  -s 98.158.157.194/27
Accept  f0_f1_udp_dst_65.51.67.112  -s 208.43.226.36

######### GCS - added by shree
Accept  f0_f1_udp_dst_65.51.67.112  -s 216.97.129.7
Accept  f0_f1_udp_dst_65.51.67.112  -s 216.97.129.11
######### added by shree for subhodh
Accept  f0_f1                       -s 173.70.76.51    -d 65.51.67.100
Accept  f0_f1                       -s 173.70.76.51    -d 65.51.67.101
Accept  f0_f1                       -s 116.68.100.20   -d 65.51.67.101
Accept  f0_f1                       -s 174.142.220.100 -d 65.51.67.101

######### IDT - added by shree
Accept  f0_f1_udp_dst_65.51.67.112  -s 216.53.4.5
Accept  f0_f1_udp_dst_65.51.67.112  -s 66.33.147.146


#########
Accept  f0_f1  -s 174.44.186.18
Accept  f0_3s  -s 174.44.186.18
Accept  f0_3s  -s 180.234.0.0/16  -d 65.51.243.124
Accept  f0_f1_udp  -d 65.51.67.124
Accept  f0_f1_udp  -d 65.51.67.125
#ccept  f0_3s      -d 65.51.243.124 -p tcp --match multiport  --dports 8585,8088
#ccept  f0_3s      -d 65.51.243.124 -p tcp --match multiport  --dports 8585,8088
#ccept  FORWARD    -s 65.51.243.124 -d 65.51.67.227  -i enp3s0f0 -o ens2f1
#ccept  f0_3s      -d 65.51.243.124 -p udp
#ccept  FORWARD    -s 65.51.67.227  -d 65.51.243.124 -i ens2f1   -o enp3s0f0

######### saiful
#ccept f0_f1 -s 103.15.140.14 -d 65.51.67.242
#ccept f0_f1 -s 24.0.88.205   -d 65.51.67.242


####### servers providing services like apache and wowza #######
Accept  f0_3s  -s 123.176.39.241/28
Accept  f0_f1  -s 112.133.204.198
Accept  f0_f1  -s 112.133.192.192/28
Accept  f0_f1  -s 112.133.195.90
Accept  f0_f1  -s 69.166.127.88      -d 65.51.67.227
Accept  f0_f1  -s 112.133.192.194
Accept  f0_f1  -s 112.133.204.16/28
######### unmetered servers
Accept  f0_f1  -s 66.55.153.140
Accept  f0_f1  -s 209.222.7.203
Accept  f0_f1  -s 216.155.155.3
Accept  f0_f1  -s 108.61.27.50
Accept  f0_f1  -s 108.61.27.51
Accept  f0_f1  -s 108.61.27.52
Accept  f0_f1  -s 108.61.27.60
Accept  f0_f1  -s 209.222.5.131
Accept  f0_f1  -s 209.222.5.219
Accept  f0_f1  -s 209.95.39.84
Accept  f0_f1  -s 206.190.152.214
Accept  f0_f1  -s 66.55.153.139
######### Florida
Accept  f0_f1  -s 209.133.205.58
Accept  f0_f1  -s 209.133.205.66
Accept  f0_f1  -s 104.156.58.26
Accept  f0_f1  -s 209.133.205.234
######### P3 ips
Accept  f0_f1  -s 123.176.38.66/28
Accept  f0_f1  -s 123.176.39.241/28
Accept  f0_f1  -s 183.82.45.192/28
### ControlS QRS servers
Accept  f0_f1  -s 45.127.100.96/30
Accept  f0_f1  -s 45.127.100.104/29
######### Registration Servers
Accept  f0_f1_udp                      -d 65.51.67.246
Accept  f0_f1_udp  -s 98.221.35.101    -d 65.51.67.249
######### XO252 for incoming
Accept  f0_f1_udp  -s 216.112.162.252  -d 65.51.67.249
######### VOIPINNOVATIONS for incoming
Accept  f0_f1_udp  -s 64.136.174.30    -d 65.51.67.249
Accept  f0_f1_udp  -s 64.136.173.31    -d 65.51.67.249
######### DID4SALE for incoming
Accept  f0_f1_udp  -s 209.216.2.211    -d 65.51.67.249
######### EXTENSIONS c/o RAVI GARU
Accept  f0_f1_udp  -s 174.44.191.112   -d 65.51.67.249

Accept  f0_f1_udp  -d 65.51.67.130
Accept  f0_f1_udp  -d 65.51.67.131
Accept  f0_f1_udp  -d 65.51.67.134

Accept  f0_f1      -d 65.51.67.4
Accept  f0_f1_udp  -d 65.51.67.41

######### Incoming Servers IPTV
Accept  f0_f1_udp  -d 65.51.67.31  -p udp --dport 5060
Accept  f0_f1_udp  -d 65.51.67.35  -p udp --dport 5060
######### Amantel
Accept  f0_f1_udp  -d 65.51.67.120 -p udp --dport 5060

Accept  f0_f1_udp  -d 65.51.67.151
Accept  f0_f1_udp  -d 65.51.67.153
Accept  f0_f1_udp  -d 65.51.67.154
Accept  f0_f1_udp  -d 65.51.67.155
Accept  f0_f1_udp  -d 65.51.67.156
Accept  f0_f1_udp  -d 65.51.67.157
Accept  f0_f1_udp  -d 65.51.67.158
Accept  f0_f1_udp  -d 65.51.67.159
Accept  f0_f1_udp  -d 65.51.67.160

######### RCC
Accept  f0_f1_udp  -d 65.51.67.73
Accept  f0_f1_udp  -d 65.51.67.74

New_chain f0_f1_dst_65.51.67.198   f0_f1  -d 65.51.67.198
Accept    f0_f1_dst_65.51.67.198  -p udp --dport 8999
Accept    f0_f1_dst_65.51.67.198  -p tcp --dport 7008
Accept    f0_f1_dst_65.51.67.198  -p tcp --dport 7009
Accept    f0_f1_dst_65.51.67.198  -s 124.253.153.164
Accept    f0_f1_dst_65.51.67.198  -s 122.175.4.203

### Channel Live ftp
Accept  f0_f1_tcp  -s 65.19.129.211 -d 65.51.67.139 -p tcp --dport 20
Accept  f0_f1_tcp  -s 65.19.129.211 -d 65.51.67.139 -p tcp --dport 21

######### Asterisk Servers Only RTP
Accept  f0_f1_udp_d10k_20k  -d 65.51.67.71

######### RCC and Misc
Accept  f0_f1_udp_d10k_20k  -d 65.51.67.67
Accept  f0_f1_udp           -d 65.51.67.67 -p udp --dport 8661
Accept  f0_f1_udp_d10k_20k  -d 65.51.67.68
Accept  f0_f1_udp_d10k_20k  -d 65.51.67.69
Accept  f0_f1_udp_d10k_20k  -d 65.51.67.70
####### Venkateswarao ILD Cust
Accept  f0_f1_tcp_d80       -d 65.51.67.88
Accept  f0_f1_udp           -d 65.51.67.88 -p udp --dport 5060
Accept  f0_f1_udp_d10k_20k  -d 65.51.67.88


######### ICMP ports and iperf
Accept  f0_f1_icmp -d 65.51.67.209

###### iperf ######
Accept  f0_f1_tcp       -d 65.51.67.22 -p tcp --dport 5001
Accept  f0_f1_udp       -d 65.51.67.22 -p udp --dport 5001
Accept  f0_f1_icmp      -d 65.51.67.22
Accept  f0_f1_tcp_decho -d 65.51.67.22
###### TCP port 53 ######
Accept  f0_f1_tcp  -d 65.51.67.75 -p tcp --dport 53
Accept  f0_f1_udp  -d 65.51.67.75 -p udp --dport 53

######### Allowed to all machines TCP Port 4019
Accept  f0_3s      -p tcp --dport 4019

######### globetele - added by shree
Accept  f0_f1_udp  -s 50.22.197.98
Accept  f0_f1_udp  -s 50.22.197.99
Accept  f0_f1_udp  -s 208.43.230.210

######### Vijay HomeIP
Accept  f0_f1      -s 124.123.192.223

######### unmetered servers
Accept  f0_f1      -s 66.55.153.187 -d 65.51.67.77
Accept  f0_f1      -s 209.222.6.179
Accept  f0_f1      -s 209.222.8.203
Accept  f0_f1      -s 209.222.9.155
######### amantel - added by shree
Accept  f0_f1_udp  -s 66.109.30.134 -d 65.51.67.112
Accept  f0_f1_udp  -s 66.109.30.134 -d 65.51.67.222

######### UN Datacenter Machines##########
New_chain  f0_f1_tcp_dst_65.51.67.215   f0_f1_tcp  -d 65.51.67.215
Accept     f0_f1_tcp_dst_65.51.67.215  -s 208.167.252.179 -p tcp --dport 8082
Accept     f0_f1_tcp_dst_65.51.67.215  -s 208.167.252.187 -p tcp --dport 8082
Accept     f0_f1_tcp_dst_65.51.67.215  -s 208.167.252.179 -p tcp --dport 80
Accept     f0_f1_tcp_dst_65.51.67.215  -s 208.167.252.187 -p tcp --dport 80

######### port forwarding to firewall on openwrt 192.168.0.26
Accept  f0_f1_tcp -d 65.51.67.227 -p tcp --dport 80

######### New Datacenter
New_chain  f0_f1_src_108.61.77.11   f0_f1  -s 108.61.77.11
Accept     f0_f1_src_108.61.77.11  -d 65.51.67.12  -p tcp --dport 3306
Accept     f0_f1_src_108.61.77.11  -d 65.51.67.105 -p tcp --dport 3306
Accept     f0_f1_src_108.61.77.11  -d 65.51.67.66  -p udp
Accept     f0_f1_src_108.61.77.11  -d 65.51.67.131 -p udp

New_chain  f0_f1_src_108.61.77.12   f0_f1  -s 108.61.77.12
Accept     f0_f1_src_108.61.77.12  -d 65.51.67.12  -p tcp --dport 3306
Accept     f0_f1_src_108.61.77.12  -d 65.51.67.91  -p tcp --dport 3306
Accept     f0_f1_src_108.61.77.12  -d 65.51.67.66  -p udp
Accept     f0_f1_src_108.61.77.12  -d 65.51.67.131 -p udp
Accept     f0_f1_src_108.61.77.12  -d 65.51.67.112 -p udp

New_chain  f0_f1_src_216.155.155.4  f0_f1  -s 216.155.155.4
Accept     f0_f1_src_216.155.155.4 -d 65.51.67.12  -p tcp --dport 3306
Accept     f0_f1_src_216.155.155.4 -d 65.51.67.91  -p tcp --dport 3306
Accept     f0_f1_src_216.155.155.4 -d 65.51.67.66  -p udp
Accept     f0_f1_src_216.155.155.4 -d 65.51.67.131 -p udp
Accept     f0_f1_src_216.155.155.4 -d 65.51.67.112 -p udp

Accept     f0_f1_udp  -s 64.237.57.53   -d 65.51.67.66
Accept     f0_f1_tcp  -s 206.190.151.8  -d 65.51.67.12 -p tcp --dport 3306
Accept     f0_f1_udp  -s 206.190.151.12 -d 65.51.67.112

######### IDT
New_chain  f0_f1_dst_65.51.67.58   f0_f1 -d 65.51.67.58
Accept     f0_f1_dst_65.51.67.58  -s 66.33.148.205
Accept     f0_f1_dst_65.51.67.58  -s 66.33.148.196
Accept     f0_f1_dst_65.51.67.58  -s 213.166.102.3
Accept     f0_f1_dst_65.51.67.58  -s 111.235.152.36
Accept     f0_f1_dst_65.51.67.58  -s 66.33.148.205  -p udp
Accept     f0_f1_dst_65.51.67.58  -s 66.33.148.196  -p udp
Accept     f0_f1_dst_65.51.67.58  -s 213.166.102.3  -p udp
Accept     f0_f1_dst_65.51.67.58  -s 111.235.152.36 -p udp

######### TV5
Accept  f0_f1_udp  -d 65.51.67.58 -p udp --match multiport --dports 9001,9000
Accept  f0_f1_udp  -d 65.51.67.33 -p udp --match multiport --dports 9002,9003,9001,9000
Accept  f0_f1_tcp  -d 65.51.67.33 -p tcp --match multiport --dports 9002,9003,9001,9000
Accept  f0_f1_tcp  -d 65.51.67.33 -p tcp --match multiport --dports 8400:8600
Accept  f0_f1_tcp  -d 65.51.67.33 -p tcp --match multiport --dports 1935,18255
Accept  f0_f1_udp  -d 65.51.67.33 -p udp --match multiport --dports 8600:8615
Accept  f0_f1_tcp  -d 65.51.67.58 -p tcp --match multiport --dports 5938,80,443


###### Manuel testing
Accept  f0_f1  -s 212.45.153.90 -d 65.51.67.118 -m state --state NEW

######### RadiantIPTV RadiantPhone
Accept  f0_f1_tcp_d443  -s 64.237.57.53 -d 65.51.67.219
Accept  f0_f1_tcp_d443  -s 64.237.57.53 -d 65.51.67.198

######### Asterisk testing
Accept  f0_f1_dst_65.51.67.198 -p udp

####### FTP #######
Accept  f0_f1_tcp  -d 65.51.67.17  -p tcp --dport 21
Accept  f0_f1_tcp  -d 65.51.67.17  -p tcp --dport 20
Accept  f0_f1_tcp  -d 65.51.67.39  -p tcp --dport 21
Accept  f0_f1_tcp  -d 65.51.67.39  -p tcp --dport 20
Accept  f0_f1      -d 65.51.67.17  -s 69.125.94.248
Accept  FORWARD    -d 65.51.67.17  -m state --state ESTABLISHED,RELATED

#########
#ccept  f0_f1_tcp  -d 65.51.67.221 -p tcp --dport 21
####### SSH #######
Accept  f0_f1_tcp  -d 65.51.67.38  -p tcp --dport 22

####### Misc ports #######
Accept  f0_f1_tcp  -d 65.51.67.20  -p tcp --dport 4545
######### Orthodox TV
Accept  f0_f1_tcp_d1935 -s 98.230.156.7 -d 65.51.67.20
#ccept  f0_f1_tcp  -d 65.51.67.8   -p tcp --dport 7226
#ccept  f0_f1_udp  -d 65.51.67.8   -p udp --dport 7226
Accept  f0_f1_tcp  -d 65.51.67.210 -p tcp --dport 3389
Accept  f0_f1_tcp  -d 65.51.67.227 -p tcp --match multiport --dports 85,6036
### for steeve
Accept  f0_f1_tcp  -d 65.51.67.8   -p tcp --dport 3011
Accept  f0_f1_tcp  -d 65.51.67.8   -p tcp --dport 1911

####### tunnel ports #######
Accept  f0_f1_udp  -d 65.51.67.20   -p udp --match multiport  --dports 20641,20642,20643
Accept  f0_f1_tcp  -d 65.51.67.20   -p tcp --match multiport  --dports 2061,2062,2063,4545

####### TCP ports 80,1935,443 #######
Accept  f0_f1_udp  -d 65.51.67.254  -p udp --dport 1935
#ccept  f0_f1_tcp_d80_1935  -d 65.51.67.77
Accept  f0_f1_tcp_d80_1935  -d 65.51.67.139
Accept  f0_f1_tcp_d80_1935  -d 65.51.67.145
Accept  f0_f1_tcp_d22       -d 65.51.67.145
Accept  f0_f1_tcp_d80_1935  -d 65.51.67.167
Accept  f0_f1_tcp_d80_1935  -d 65.51.67.129
Accept  f0_f1_tcp_d80_1935  -d 65.51.67.209
Accept  f0_f1_tcp_d80_1935  -d 65.51.67.245
######## bangla  server
Accept  f0_f1_tcp_d80_1935  -d 65.51.67.21

Accept  f0_f1_tcp_d80       -d 65.51.67.114
Accept  f0_f1_tcp_d80_1935  -d 65.51.67.50
Accept  f0_f1_tcp_d80       -d 65.51.67.46
Accept  f0_f1_tcp_d80_1935  -d 65.51.67.188

Accept  f0_f1_tcp_d80       -d 65.51.67.168
Accept  f0_f1_tcp_d80       -d 65.51.67.191
Accept  f0_f1_tcp_d80       -d 65.51.67.194
Accept  f0_f1_tcp_d80       -d 65.51.67.57
Accept  f0_f1_tcp_d80       -d 65.51.67.123
Accept  f0_f1_tcp_d80       -d 65.51.67.56
Accept  f0_f1_tcp_d80       -d 65.51.67.27
Accept  f0_f1_tcp_d80       -d 65.51.67.140
Accept  f0_f1_tcp_d80       -d 65.51.67.17
Accept  f0_f1_tcp_d80       -d 65.51.67.19
Accept  f0_f1_tcp_d80       -d 65.51.67.24
Accept  f0_f1_tcp_d80       -d 65.51.67.25

Accept  f0_f1_tcp_d80_1935  -d 65.51.67.47
Accept  f0_f1_tcp           -d 65.51.67.80  -p tcp --match multiport  --dports 1101,1102
Accept  f0_f1_tcp_d80       -d 65.51.67.22
Accept  f0_f1_tcp_d80       -d 65.51.67.142
#ccept  f0_f1_tcp_d1935     -d 65.51.67.142

Accept  f0_f1_tcp_d80       -d 65.51.67.226
Accept  f0_f1_tcp_d80       -d 65.51.67.242
Accept  f0_f1_tcp_d80       -d 65.51.67.243
Accept  f0_f1_udp           -d 65.51.67.243
Accept  f0_f1_tcp_d80       -d 65.51.67.5
Accept  f0_f1_tcp_d80       -d 65.51.67.7
Accept  f0_f1_tcp_d80       -d 65.51.67.164
Accept  f0_f1_tcp_d80       -d 65.51.67.77
Accept  f0_f1_tcp_d80       -d 65.51.67.169
Accept  f0_f1_tcp_d80       -d 65.51.67.170
Accept  f0_f1_tcp_d80       -d 65.51.67.51
Accept  f0_f1_tcp_d80       -d 65.51.67.52
Accept  f0_f1_tcp_d80_1935  -d 65.51.67.40
Accept  f0_f1_tcp_d80_1935  -d 65.51.67.254
Accept  f0_f1_tcp_d80       -d 65.51.67.32
Accept  f0_f1_tcp_d80       -d 65.51.67.33
Accept  f0_f1_tcp_d80       -d 65.51.67.15
Accept  f0_f1_tcp_d80       -d 65.51.67.188
Accept  f0_3s               -d 65.51.243.125 -p tcp --dport 80

New_chain  f0_f1_dst_65.51.67.97   f0_f1  -d 65.51.67.97
Accept     f0_f1_dst_65.51.67.97  -p tcp --dport 80
Accept     f0_f1_dst_65.51.67.97  -p tcp --dport 8080
Accept     f0_f1_dst_65.51.67.97  -p tcp --dport 443
Accept     f0_f1_dst_65.51.67.97  -p tcp --dport 90
Accept     f0_f1_dst_65.51.67.97  -p tcp --dport 1883
Accept     f0_f1_dst_65.51.67.97  -p udp --dport 3478
Accept     f0_f1_dst_65.51.67.97  -p udp --dport 3479
Accept     f0_f1_dst_65.51.67.97  -p tcp --dport 9001

Accept  f0_f1_tcp_d80     -d 65.51.243.119
Accept  f0_f1_tcp_d80     -d 65.51.67.13
Accept  f0_f1_tcp_d80     -d 65.51.67.144
Accept  f0_f1_tcp_d80     -d 65.51.67.14
Accept  f0_f1_tcp_d80     -d 65.51.67.130
Accept  f0_f1_tcp_d80     -d 65.51.67.201
Accept  f0_f1_tcp_d80     -d 65.51.67.202
Accept  f0_f1_tcp_d80     -d 65.51.67.203
Accept  f0_f1_tcp_d80     -d 65.51.67.206
Accept  f0_f1_tcp_d80     -d 65.51.67.207
Accept  f0_f1_tcp_d80     -d 65.51.67.12
Accept  f0_f1_tcp_d80     -d 65.51.67.49
Accept  f0_f1_tcp_d90     -d 65.51.67.49
Accept  f0_f1_tcp_d1935   -d 65.51.67.49
Accept  f0_f1_tcp_d80     -d 65.51.67.135
Accept  f0_f1_tcp_d90     -d 65.51.67.135
Accept  f0_f1_tcp_d80     -d 65.51.67.211
Accept  f0_f1_tcp_d80     -d 65.51.67.221
Accept  f0_f1_tcp_d80     -d 65.51.67.224

Accept  f0_f1_tcp_d80     -d 65.51.67.59
Accept  f0_f1_tcp_d443    -d 65.51.67.59
Accept  f0_f1_tcp_d80     -d 65.51.67.253
Accept  f0_f1_tcp_d80     -d 65.51.67.247
Accept  f0_f1_tcp_d90     -d 65.51.67.247
Accept  f0_f1_tcp_d1935   -d 65.51.67.247
Accept  f0_f1_tcp_d80_443 -d 65.51.67.215
Accept  f0_f1_tcp_d80_443 -d 65.51.67.117
Accept  f0_f1_tcp_d80_443 -d 65.51.67.61
Accept  f0_f1_tcp_d80_443 -d 65.51.67.205
Accept  f0_f1_tcp_d80_443 -d 65.51.67.204
Accept  f0_f1_tcp_d80_443 -d 65.51.67.216
Accept  f0_f1_icmp        -d 65.51.67.216
Accept  f0_f1_tcp_decho   -d 65.51.67.216
Accept  f0_f1_tcp_d80_443 -d 65.51.67.246
Accept  f0_f1_tcp_d80_443 -d 65.51.67.217
Accept  f0_f1_tcp         -d 65.51.67.218 -p tcp --match multiport  --dports 587,25,993,443,465,143,80
Accept  f0_f1_tcp_d80_443 -d 65.51.67.147

Accept  f0_f1_tcp_d80_1935 -d 65.51.67.127

Accept  f0_f1_tcp_d80   -d 65.51.67.134

######### Mango mobile
New_chain f0_f1_dst_65.51.67.147   f0_f1  -d 65.51.67.147
Accept    f0_f1_dst_65.51.67.147  -s 98.223.181.162
Accept    f0_f1_dst_65.51.67.147  -s 49.205.54.166
Accept    f0_f1_dst_65.51.67.147  -s 123.176.46.21
Accept    f0_f1_dst_65.51.67.147  -s 173.165.87.169
Accept    f0_f1_dst_65.51.67.147  -s 202.153.45.190
Accept    f0_f1_dst_65.51.67.147  -s 49.205.39.126

New_chain f0_f1_dst_65.51.67.254   f0_f1  -d 65.51.67.254
Accept    f0_f1_dst_65.51.67.254  -s 49.205.39.126
Accept    f0_f1_dst_65.51.67.254  -s 173.165.87.169
Accept    f0_f1_dst_65.51.67.254  -s 75.15.14.251
Accept    f0_f1_dst_65.51.67.254  -s 202.153.45.190
Accept    f0_f1_dst_65.51.67.254  -s 23.176.46.19
Accept    f0_f1_dst_65.51.67.254  -s 23.176.46.21
Accept    f0_f1_dst_65.51.67.254  -s 202.153.45.14
Accept    f0_f1_dst_65.51.67.254  -s 202.153.36.192
Accept    f0_f1_dst_65.51.67.254  -s 202.153.46.146
Accept    f0_f1_dst_65.51.67.254  -s 202.153.46.149
Accept    f0_f1_dst_65.51.67.254  -s 202.153.46.150
Accept    f0_f1_dst_65.51.67.254  -s 202.153.45.190

Accept    f0_f1_tcp_d80       -d 65.51.67.198
Accept    f0_f1_tcp_d80       -d 65.51.67.7
Accept    f0_f1_tcp           -d 65.51.67.20  -p tcp --match multiport  --dports 80,90,1723,1935

Accept    f0_f1_tcp_d80_1935  -d 65.51.67.15
Accept    f0_f1_tcp_d80_1935  -d 65.51.67.252
Accept    f0_f1_tcp           -d 65.51.67.15  -s 5.152.210.134
Accept    f0_f1_tcp_d80_1935  -d 65.51.67.19
Accept    f0_f1_tcp_d1935     -d 65.51.67.25

Accept    f0_f1_tcp_d80       -d 65.51.67.180
Accept    f0_f1_tcp_d80_443   -d 65.51.67.214
Accept    f0_f1_tcp_d80_443   -d 65.51.67.210
Accept    f0_f1_tcp_d80_443   -d 65.51.67.220
Accept    f0_f1_tcp_d80_443   -d 65.51.67.141
Accept    f0_f1_tcp_d80_443   -d 65.51.67.147
Accept    f0_f1_tcp_d80       -d 65.51.67.8

######### access for openwrt equipment
Accept f0_f1_tcp     -d 65.51.67.142 -p tcp --dport 22

###### Prasad Gangavarapu
Accept    f0_f1_tcp_d22      -s 119.235.52.220 -d 65.51.67.215
Accept    f0_f1_tcp_d80_443  -s 119.235.52.220 -d 65.51.67.215
Accept    f0_f1_tcp_d22      -s 119.235.52.220 -d 65.51.67.198
Accept    f0_f1_tcp_d80_443  -s 119.235.52.220 -d 65.51.67.198

### TFTP Rules
New_chain f0_f1_dst_65.51.67.165   f0_f1  -d 65.51.67.165
Accept    f0_f1_dst_65.51.67.165  -p udp -m udp --dport 69
Accept    f0_f1_dst_65.51.67.165  -p udp -m udp --dport 5060
Accept    f0_f1_dst_65.51.67.165  -p udp
Accept    f0_f1_dst_65.51.67.165  -p tcp --match multiport --dports 80,443

#####################
Accept    f0_3s  -s 183.82.45.192/28
Accept    f0_3s  -s 183.82.45.193/28
Accept    f0_f1  -s 24.187.17.116
Accept    f0_3s  -s 63.243.204.32/27
Accept    f0_f1  -s 63.243.204.32/27
######### saju
Accept    f0_f1  -s 98.226.82.133   -d 65.51.67.20
Accept    f0_f1  -s 98.226.82.133   -d 65.51.67.253
Accept    f0_f1  -s 67.80.119.23    -d 65.51.67.49
######### BollyWorld
Accept    f0_f1  -s 50.151.140.46   -d 65.51.67.247
Accept    f0_f1  -s 174.142.220.100 -d 65.51.67.247
Accept    f0_f1  -s 108.5.148.35    -d 65.51.67.247

######### desi plaza ip
Accept    f0_f1  -s 76.187.66.18    -d 65.51.67.17

########## tvasia
Accept    f0_f1  -s 24.38.127.100
Accept    f0_f1  -s 203.92.35.94    -d 65.51.67.20
Accept    f0_f1  -s 203.92.35.91    -d 65.51.67.20

### Railtel Monitoring
Accept    f0_f1_icmp  -s 119.235.48.5 -d 65.51.67.209

######### end of servers list for iptv

Accept    f0_f1   -s 76.187.66.18 -d 65.51.67.17

########## sunil home ip
Accept    f0_f1   -s 69.243.40.180
######### access to mmtv server for sunil
New_chain f0_f1_dst_65.51.67.141   f0_f1  -d 65.51.67.141
Accept    f0_f1_dst_65.51.67.141  -s 64.26.197.152
Accept    f0_f1_dst_65.51.67.141  -s 76.187.88.36
Accept    f0_f1_dst_65.51.67.141  -s 76.187.65.39
Accept    f0_f1_dst_65.51.67.141  -s 49.205.174.62
Accept    f0_f1_dst_65.51.67.141  -s 183.82.99.167
Drop      f0_f1  -s 24.0.93.6     -d 65.51.67.252
Accept    f0_f1  -s 69.175.48.234 -d 65.51.67.195
######### port 6675 testing
Accept    f0_f1  -s 68.44.99.39   -d 65.51.67.20

######### rules for col server from delta three and col office
Accept    f0_f1  -s 213.137.73.140  -d 65.51.67.208
Accept    f0_f1  -s 213.137.83.61   -d 65.51.67.208
Accept    f0_f1  -s 202.62.64.35    -d 65.51.67.208
Accept    f0_f1  -s 202.62.64.35    -d 65.51.67.112
Accept    f0_f1  -s 202.62.64.202   -d 65.51.67.208
Accept    f0_f1  -s 49.156.155.0/24 -d 65.51.67.208
Accept    f0_f1  -s 202.62.64.0/24  -d 65.51.67.208

#### COL
Accept    f0_f1  -s 202.62.66.168/29

Accept    f0_3s  -s 59.90.209.162   -d 65.51.243.116
Accept    f0_f1  -s 202.62.85.7     -d 65.51.67.206
Accept    f0_f1  -s 115.111.24.77
Accept    f0_f1  -s 173.77.183.61   -d 65.51.67.5
Accept    f0_f1  -s 67.203.4.26     -d 65.51.67.5
Accept    f0_f1  -s 72.9.145.51     -d 65.51.67.5
Accept    f0_f1  -s 174.57.41.225
Accept    f0_f1  -s 218.17.162.159  -d 65.51.67.13
Accept    f0_f1  -s 68.192.189.151
Accept    f0_3s  -s 68.192.189.151
Accept    f0_3s  -s 174.57.41.225
Accept    f0_f1  -s 182.18.179.52
Accept    f0_3s  -s 182.18.179.52

Accept  f0_f1     -s 183.82.45.195/28
Accept  f0_f1_udp -s 107.6.17.75
Accept  f0_f1_udp -s 71.125.74.202
Accept  f0_f1_udp -s 81.201.82.128/26
Accept  f0_f1_udp -s 81.201.83.0/26
Accept  f0_f1_udp -s 81.201.84.0/26
Accept  f0_f1_udp -s 81.201.84.128/26
Accept  f0_f1_udp -s 81.201.86.0/26

Accept  f0_f1     -s 89.28.90.188
Accept  f0_3s     -s 89.28.90.188
Accept  f0_f1     -s 67.88.58.53
Accept  f0_3s     -s 67.88.58.53
#ccept  f0_3s     -s 166.137.136.19 -d 65.51.243.126

######### Shiva sir
#ccept  f0_3s     -s 183.82.90.171  -d 65.51.243.126

###########
#ccept  f0_3s     -s 67.171.19.72   -d 65.51.243.126
#ccept  f0_3s     -s 216.53.8.130   -d 65.51.243.126
Accept  f0_f1     -s 68.236.223.242 -d 65.51.67.74
Accept  f0_f1_udp -s 24.185.45.11   -d 65.51.67.74

######### ace clinic
Accept  f0_f1     -s 96.232.178.13  -d 65.51.67.74

######### Srikanth Bandla
#Accept f0_3s -s 76.117.238.114 -d 65.51.243.126

################
Accept  f0_f1      -s 216.112.162.252 -d 65.51.67.74
Accept  f0_f1      -s 216.112.162.252 -d 65.51.67.73
Accept  f0_f1      -s 24.38.127.97    -d 65.51.67.130

Accept  f0_f1_udp  -s 207.155.147.62  -d 65.51.67.55
Accept  f0_f1_udp  -s 207.155.147.62  -d 65.51.67.58
Accept  f0_f1_udp  -s 80.77.12.195    -d 65.51.67.112
Accept  f0_f1_udp  -s 80.77.12.171    -d 65.51.67.112
Accept  f0_f1_udp  -s 80.77.12.171
Accept  f0_f1_tcp  -s 203.81.109.3    -d 65.51.67.212 -p tcp --dport 3306

######### Ramesh
New_chain f0_f1_src_183.82.0.0  f0_f1 -s 183.82.0.0/16
Accept    f0_f1_src_183.82.0.0 -d 65.51.67.6   -p tcp --dport 80
Accept    f0_f1_src_183.82.0.0 -d 65.51.67.150 -p tcp --dport 80
Accept    f0_f1_src_183.82.0.0 -d 65.51.67.129 -p tcp --match multiport --dports 8086,8088
Accept    f0_f1_src_183.82.0.0 -d 65.51.67.167 -p tcp --match multiport --dports 8086,8088
Accept    f0_f1_src_183.82.0.0 -d 65.51.67.245 -p tcp --match multiport --dports 8086,8088
Accept    f0_f1_src_183.82.0.0 -d 65.51.67.22  -p tcp --match multiport --dports 80,1935
Accept    f0_f1_src_183.82.0.0 -d 65.51.67.112 -p tcp --match multiport --dports 80,443

Accept    f0_3s      -s 76.98.4.1
Accept    f0_f1_udp  -s 71.91.164.123  -d 65.51.67.74
Accept    f0_f1_udp  -s 117.201.197.54 -d 65.51.67.74
Accept    f0_f1_udp  -s 69.127.17.239  -d 65.51.67.74
Accept    f0_f1_udp  -s 117.242.16.74  -d 65.51.67.74
Accept    f0_f1      -s 217.73.86.206  -d 65.51.67.78

Accept    f0_3s      -s 148.87.19.202  -d 65.51.67.30
Accept    f0_3s      -s 98.224.28.69   -d 65.51.67.30
Accept    f0_f1      -s 98.224.28.69   -d 65.51.67.30
Accept    f0_f1      -s 76.116.181.99
Accept    f0_3s      -s 76.116.181.99
Accept    f0_3s      -s 66.249.68.89   -d 65.51.67.30
Accept    f0_f1      -s 66.249.68.89   -d 65.51.67.30
Accept    f0_3s      -s 76.98.1.156    -d 65.51.67.30
Accept    f0_f1      -s 76.98.1.156    -d 65.51.67.30
Accept    f0_f1      -s 24.189.92.71   -d 65.51.67.180
Accept    f0_f1      -s 115.248.86.171 -d 65.51.67.18
Accept    f0_f1      -s 183.82.42.24/29
Accept    f0_f1      -s 76.116.176.230
Accept    f0_3s      -s 76.116.176.230
Accept    f0_f1      -s 98.224.28.69
Accept    f0_3s      -s 68.192.182.116
Accept    f0_f1      -s 74.196.252.51
Accept    f0_f1      -s 68.32.234.211
Accept    f0_f1      -s 69.74.152.30
Accept    f0_f1      -s 68.192.182.116
Accept    f0_f1      -s 76.124.23.64
Accept    f0_3s      -s 76.124.23.64
Accept    f0_3s      -s 96.57.108.90 -d 65.51.243.117 -p tcp -m tcp --dport 1022
Accept    f0_3s      -s 99.10.223.152
Accept    f0_f1      -s 69.248.69.46
Accept    f0_3s      -s 69.248.69.46
Accept    f0_f1      -s 69.142.169.96
Accept    f0_3s      -s 69.142.169.96
Accept    f0_f1      -s 69.142.168.62
Accept    f0_3s      -s 69.142.168.62
Accept    f0_f1      -s 173.251.82.0/26
Accept    f0_3s      -s 173.251.82.0/26
Accept    f0_f1      -s 174.57.41.79
Accept    f0_f1      -s 75.142.172.76 -d 65.51.67.40
Accept    f0_f1      -s 76.187.78.98  -d 65.51.67.40
####### AMANTEL SERVERS #######
Accept    f0_f1_tcp_d80  -d 65.51.67.122
Accept    f0_f1_tcp_d443 -d 65.51.67.122
### Informed by Mr. Shree
Accept    f0_f1_tcp -p tcp --dport 8111
Accept    f0_f1_udp -p udp --dport 8111

######### Ravi mqtt ports
Accept    f0_f1_tcp   -d 65.51.67.124 -p tcp --match multiport --dports 1883,8883,8884
Accept    f0_f1_tcp   -d 65.51.67.125 -p tcp --match multiport --dports 1883,8883,8884

New_chain f0_f1_src_124.124.44.108   f0_f1 -s 124.124.44.108
Accept    f0_f1_src_124.124.44.108
Accept    f0_f1_src_124.124.44.108  -d 65.51.67.121
Accept    f0_f1_src_124.124.44.108  -d 65.51.67.72
Accept    f0_f1_src_124.124.44.108  -d 65.51.67.121
Accept    f0_f1_src_124.124.44.108  -d 65.51.67.83
Accept    f0_f1_src_124.124.44.108  -d 65.51.67.107
Accept    f0_f1_src_124.124.44.108  -d 65.51.67.108
Accept    f0_f1_src_124.124.44.108  -d 65.51.67.111
Accept    f0_f1_src_124.124.44.108  -d 65.51.67.115
Accept    f0_f1_src_124.124.44.108  -d 65.51.67.118
Accept    f0_f1_src_124.124.44.108  -d 65.51.67.151
Accept    f0_f1_src_124.124.44.108  -d 65.51.67.153
Accept    f0_f1_src_124.124.44.108  -d 65.51.67.154
Accept    f0_f1_src_124.124.44.108  -d 65.51.67.155
Accept    f0_f1_src_124.124.44.108  -d 65.51.67.156
Accept    f0_f1_src_124.124.44.108  -d 65.51.67.157
Accept    f0_f1_src_124.124.44.108  -d 65.51.67.158
Accept    f0_f1_src_124.124.44.108  -d 65.51.67.159
Accept    f0_f1_src_124.124.44.108  -d 65.51.67.160
Accept    f0_f1_src_124.124.44.108  -d 65.51.67.104
Accept    f0_f1_src_124.124.44.108  -d 65.51.67.152

####### Mango Mobile TV #######
Accept    f0_f1  -s 183.83.227.38
Accept    f0_f1  -s 183.83.237.123
Accept    f0_f1  -s 183.82.221.186

######### Natcom (Lakshman Rao Garu)
Accept    f0_f1  -s 113.193.184.126 -d 65.51.67.18
Accept    f0_f1  -s 24.188.46.163   -d 65.51.67.18

####### Coolech #######
Accept    f0_f1  -s 218.17.162.159
Drop    FORWARD  -s 24.191.111.252
Drop    FORWARD  -s 24.87.36.24

## END ##
